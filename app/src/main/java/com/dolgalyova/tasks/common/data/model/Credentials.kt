package com.dolgalyova.tasks.common.data.model

data class Credentials(val email: String, val password: String)