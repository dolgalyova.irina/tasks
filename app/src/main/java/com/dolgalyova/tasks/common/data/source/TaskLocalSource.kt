package com.dolgalyova.tasks.common.data.source

import com.dolgalyova.tasks.common.data.model.TaskInfo

interface TaskLocalSource {

    fun getAll(): List<TaskInfo>

    fun getWithReminder(): List<TaskInfo>

    fun getNotUploaded(): List<TaskInfo>

    fun getById(id: Int): TaskInfo

    fun put(task: TaskInfo): TaskInfo

    fun delete(id: Int)
}