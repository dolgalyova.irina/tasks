package com.dolgalyova.tasks.common.data.source.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dolgalyova.tasks.common.data.source.db.dao.TaskDao
import com.dolgalyova.tasks.common.data.source.db.entity.TaskEntity

@Database(entities = [TaskEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract val taskDao: TaskDao
}