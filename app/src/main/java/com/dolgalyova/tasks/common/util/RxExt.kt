package com.dolgalyova.tasks.common.util

import com.dolgalyova.tasks.common.arch.RxWorkers
import io.reactivex.*

fun <T> Single<T>.composeWith(workers: RxWorkers) = compose {
    it.subscribeOn(workers.subscribeWorker).observeOn(workers.observeWorker)
}

fun Completable.composeWith(workers: RxWorkers) = compose {
    it.subscribeOn(workers.subscribeWorker).observeOn(workers.observeWorker)
}

fun <T> Observable<T>.composeWith(workers: RxWorkers) = compose {
    it.subscribeOn(workers.subscribeWorker).observeOn(workers.observeWorker)
}

fun <T> Maybe<T>.composeWith(workers: RxWorkers) = compose {
    it.subscribeOn(workers.subscribeWorker).observeOn(workers.observeWorker)
}

fun <T> Flowable<T>.composeWith(workers: RxWorkers) = compose {
    it.subscribeOn(workers.subscribeWorker).observeOn(workers.observeWorker)
}