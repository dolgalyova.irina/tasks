package com.dolgalyova.tasks.common.arch

import io.reactivex.Scheduler

data class RxWorkers(
    val subscribeWorker: Scheduler,
    val observeWorker: Scheduler
)