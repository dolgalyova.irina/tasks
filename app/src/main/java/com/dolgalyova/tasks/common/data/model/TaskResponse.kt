package com.dolgalyova.tasks.common.data.model

data class TaskResponse(val task: Task)