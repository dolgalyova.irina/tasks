package com.dolgalyova.tasks.common.data.source

import com.dolgalyova.tasks.common.data.exception.ItemDoesntExistException
import com.dolgalyova.tasks.common.data.model.Priority
import com.dolgalyova.tasks.common.data.model.TaskInfo
import com.dolgalyova.tasks.common.data.source.db.dao.TaskDao
import com.dolgalyova.tasks.common.data.source.db.entity.TaskEntity
import java.util.*

class TaskRoomSource(private val dao: TaskDao) : TaskLocalSource {

    override fun getAll(): List<TaskInfo> {
        return dao.readAll().map { it.toTask() }
    }

    override fun getById(id: Int): TaskInfo {
        return dao.getById(id)?.toTask() ?: throw ItemDoesntExistException()
    }

    override fun put(task: TaskInfo): TaskInfo {
        dao.create(task.toEntity())
        return task
    }

    override fun delete(id: Int) {
        dao.getById(id)?.let { dao.delete(it) }
    }

    override fun getWithReminder(): List<TaskInfo> = dao.getWithReminders().map { it.toTask() }

    override fun getNotUploaded(): List<TaskInfo> = dao.getNotUploaded().map { it.toTask() }

    private fun TaskEntity.toTask(): TaskInfo {
        val priority =
                if (this.priority >= 0) Priority.values()[this.priority]
                else null
        return TaskInfo(this.id, this.title, Date(this.dueBy), priority, this.hasReminder, this.isUploaded)
    }

    private fun TaskInfo.toEntity(): TaskEntity {
        return TaskEntity(this.id, this.title, this.dueDate.time,
                this.priority?.ordinal ?: -1, this.hasReminder, this.isUploaded)
    }
}