package com.dolgalyova.tasks.common.data.exception

class ItemDoesntExistException : IllegalArgumentException()