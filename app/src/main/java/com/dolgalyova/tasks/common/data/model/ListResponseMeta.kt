package com.dolgalyova.tasks.common.data.model

data class ListResponseMeta(val current: Int,
                            val limit: Int,
                            val count: Int)