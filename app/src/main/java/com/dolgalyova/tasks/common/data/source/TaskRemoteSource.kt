package com.dolgalyova.tasks.common.data.source

import com.dolgalyova.tasks.common.data.model.CreateTaskBody
import com.dolgalyova.tasks.common.data.model.Task
import com.dolgalyova.tasks.common.data.model.TaskListResponse
import com.dolgalyova.tasks.common.data.model.TaskResponse
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*

interface TaskRemoteSource {

    @GET("tasks")
    fun getAll(@Query("page") page: Int,
               @Query("sort") sort: String): Single<TaskListResponse>

    @GET("tasks/{taskId}")
    fun getById(@Path("taskId") id: Int): Single<TaskResponse>

    @POST("tasks")
    fun create(@Body task: CreateTaskBody): Single<TaskResponse>

    @PUT("tasks/{taskId}")
    fun update(@Path("taskId") id: Int, @Body task: Task): Completable

    @DELETE("tasks/{taskId}")
    fun deleteTask(@Path("taskId") id: Int): Completable
}