package com.dolgalyova.tasks.common.data.repository

import com.dolgalyova.tasks.common.data.model.*
import com.dolgalyova.tasks.common.data.source.TaskLocalSource
import com.dolgalyova.tasks.common.data.source.TaskRemoteSource
import com.dolgalyova.tasks.common.util.getOrNull
import io.reactivex.Completable
import io.reactivex.Single
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit

class TaskLocalRestRepository(private val remoteSource: TaskRemoteSource,
                              private val localSource: TaskLocalSource) : TaskRepository {

    override fun create(task: TaskInfo): Single<TaskInfo> {
        val time = TimeUnit.MILLISECONDS.toSeconds(task.dueDate.time)
        return remoteSource.create(CreateTaskBody(task.title, time, task.priority?.name))
                .map { it.task.toTaskInfo(task.hasReminder, isUploaded = true) }
                .doOnSuccess { localSource.put(it) }
                .doOnError { localSource.put(task.copy(isUploaded = false)) }
    }

    override fun getById(id: Int): Single<TaskInfo> {
        return Single.fromCallable { localSource.getById(id) }
                .onErrorResumeNext {
                    remoteSource.getById(id)
                            .map { it.task.toTaskInfo(hasReminder = false, isUploaded = true) }
                            .doOnSuccess { localSource.put(it) }
                }
    }

    override fun getAll(page: Int, sort: SortCriteria): Single<List<TaskInfo>> {
        return remoteSource.getAll(page, sort.toSqlCriteria().toLowerCase())
                .doOnSuccess { response ->
                    response.tasks.forEach {
                        val hasReminder = getOrNull { localSource.getById(it.id) }?.hasReminder
                                ?: false
                        localSource.put(it.toTaskInfo(hasReminder = hasReminder, isUploaded = true))
                    }
                }
                .onErrorReturnItem(TaskListResponse.EMPTY)
                .map { localSource.getAll() }
                .map { tasks -> tasks.distinctBy { it.id } }
                .map { tasks ->
                    val result = when (sort.field) {
                        SortField.TITLE -> tasks.sortedBy { it.title }
                        SortField.DATE -> tasks.sortedBy { it.dueDate }
                        SortField.PRIORITY -> tasks.sortedBy { it.priority }
                    }
                    if (sort.type == SortType.ASC) result
                    else result.asReversed()
                }
    }

    override fun getTasksWithReminder(): Single<List<TaskInfo>> {
        return Single.fromCallable { localSource.getWithReminder() }
    }

    override fun getNotUploadedTasks(): Single<List<TaskInfo>> {
        return Single.fromCallable { localSource.getNotUploaded() }
    }

    override fun update(task: TaskInfo): Completable {
        val body = task.toTask()
        return remoteSource.update(task.id, body)
                .doOnComplete { localSource.put(task) }
    }

    override fun delete(id: Int): Completable {
        return Completable.fromAction { localSource.delete(id) }
                .andThen(remoteSource.deleteTask(id))
    }
}

private fun Task.toTaskInfo(hasReminder: Boolean, isUploaded: Boolean): TaskInfo {
    return TaskInfo(this.id, this.title, Date(TimeUnit.SECONDS.toMillis(this.dueBy)),
            this.priority, hasReminder, isUploaded)
}

private fun TaskInfo.toTask(): Task {
    return Task(this.id, this.title, TimeUnit.MILLISECONDS.toSeconds(this.dueDate.time), this.priority)
}