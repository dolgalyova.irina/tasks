package com.dolgalyova.tasks.common.data.worker

import com.dolgalyova.tasks.common.di.WorkerScope
import dagger.Subcomponent

@WorkerScope
@Subcomponent
interface SyncTasksComponent {

    fun inject(target: SyncTasksWorker)

    @Subcomponent.Builder
    interface Builder {
        fun build(): SyncTasksComponent
    }

    interface ComponentProvider {
        fun provideSyncTasksComponent(): SyncTasksComponent
    }
}