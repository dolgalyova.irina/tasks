package com.dolgalyova.tasks.common.data.worker

import android.content.Context
import androidx.work.*
import com.dolgalyova.tasks.common.data.repository.TaskRepository
import com.dolgalyova.tasks.common.util.getOrNull
import javax.inject.Inject

class SyncTasksWorker(context: Context, params: WorkerParameters) : Worker(context, params) {

    companion object {
        fun start() {
            val constraints = Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()
            val request = OneTimeWorkRequestBuilder<SyncTasksWorker>()
                    .setConstraints(constraints)
                    .addTag(SyncTasksWorker::class.java.simpleName)
                    .build()
            WorkManager.getInstance()
                    .beginUniqueWork(SyncTasksWorker::class.java.simpleName, ExistingWorkPolicy.KEEP, request)
                    .enqueue()
        }
    }

    private val component by lazy {
        (applicationContext as SyncTasksComponent.ComponentProvider).provideSyncTasksComponent()
    }
    @Inject lateinit var repository: TaskRepository

    override fun doWork(): Result {
        if (!::repository.isInitialized) component.inject(this)

        val tasks = getOrNull { repository.getNotUploadedTasks().blockingGet() }.orEmpty()
        tasks.forEach { task ->
            getOrNull { repository.delete(task.id).blockingAwait() }
            getOrNull { repository.create(task).blockingGet() }
        }
        return Result.success()
    }
}