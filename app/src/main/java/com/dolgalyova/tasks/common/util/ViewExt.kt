package com.dolgalyova.tasks.common.util

import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.setLoadMoreListener(onLoadNext: () -> Unit) {
    this.addOnScrollListener(LoadMoreScrollListener(onLoadNext))
}