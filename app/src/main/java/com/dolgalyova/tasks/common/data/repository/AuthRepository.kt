package com.dolgalyova.tasks.common.data.repository

import com.dolgalyova.tasks.common.data.model.Credentials
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRepository {

    fun login(credentials: Credentials): Completable

    fun register(credentials: Credentials): Completable

    fun isUserAuthorized():Single<Boolean>
}