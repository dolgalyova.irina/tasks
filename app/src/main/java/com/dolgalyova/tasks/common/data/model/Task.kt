package com.dolgalyova.tasks.common.data.model

data class Task(val id: Int,
                val title: String,
                val dueBy: Long,
                val priority: Priority?)