package com.dolgalyova.tasks.common.data

import com.dolgalyova.tasks.common.data.source.AuthLocalStorage
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(private val storage: AuthLocalStorage) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        if (storage.token.isNullOrBlank()) return chain.proceed(chain.request())

        val original = chain.request()
        val originalHeaders = original.headers()
        val interceptedHeaders = originalHeaders.newBuilder()
                .add("Authorization", "Bearer: ${storage.token}")
                .build()
        val interceptedRequest = original.newBuilder()
                .headers(interceptedHeaders)
                .build()
        return chain.proceed(interceptedRequest)
    }
}