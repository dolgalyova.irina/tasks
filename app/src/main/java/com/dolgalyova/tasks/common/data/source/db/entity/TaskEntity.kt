package com.dolgalyova.tasks.common.data.source.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tasks")
data class TaskEntity(@PrimaryKey val id: Int,
                      val title: String,
                      val dueBy: Long,
                      val priority: Int,
                      val hasReminder: Boolean,
                      val isUploaded: Boolean)