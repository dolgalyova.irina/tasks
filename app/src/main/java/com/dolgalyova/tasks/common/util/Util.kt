package com.dolgalyova.tasks.common.util

import com.dolgalyova.tasks.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*

inline fun <T> getOrNull(body: () -> T?) = try {
    body()
} catch (e: Exception) {
    null
}

fun <T> createApiService(clazz: Class<T>, client: OkHttpClient): T {
    return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(clazz)
}

private const val READABLE_DATE_FORMAT = "EEEE, dd MM, yyyy"
fun Date.toReadableDate(): String {
    val formatter = SimpleDateFormat(READABLE_DATE_FORMAT, Locale.getDefault())
    return getOrNull { formatter.format(this) }.orEmpty()
}

fun String.parseReadableDate(): Date? {
    val formatter = SimpleDateFormat(READABLE_DATE_FORMAT, Locale.getDefault())
    return getOrNull { formatter.parse(this) }
}

fun Throwable.isNoInternetError(): Boolean {
    return this is UnknownHostException || this is SocketTimeoutException || this is ConnectException
}