package com.dolgalyova.tasks.common.data.model

data class SortCriteria(val field: SortField, val type: SortType) {

    fun toSqlCriteria() = "${field.toSqlCriteria()} ${type.toSqlCriteria()}"
}

enum class SortField {
    TITLE, DATE, PRIORITY;

    fun toSqlCriteria() = when (this) {
        TITLE -> "title"
        DATE -> "dueBy"
        PRIORITY -> "priority"
    }
}

enum class SortType {
    ASC, DESC;

    fun toSqlCriteria() = this.name.toUpperCase()
}