package com.dolgalyova.tasks.common.data.model

data class CreateTaskBody(val title: String,
                          val dueBy: Long,
                          val priority: String?)