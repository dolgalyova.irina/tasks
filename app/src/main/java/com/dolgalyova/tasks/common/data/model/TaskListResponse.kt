package com.dolgalyova.tasks.common.data.model

data class TaskListResponse(val tasks: List<Task>,
                            val meta: ListResponseMeta) {
    companion object {
        val EMPTY = TaskListResponse(emptyList(), ListResponseMeta(0, 0, 0))
    }
}