package com.dolgalyova.tasks.common.data.source

import android.content.SharedPreferences
import com.dolgalyova.tasks.BuildConfig

class AuthPreferencesStorage(private val preferences: SharedPreferences) : AuthLocalStorage {
    private companion object {
        const val KEY_TOKEN = "${BuildConfig.APPLICATION_ID}.KEY_TOKEN"
    }

    override var token: String?
        get() = preferences.getString(KEY_TOKEN, "").orEmpty()
        set(value) {
            preferences.edit().putString(KEY_TOKEN, value).apply()
        }
}