package com.dolgalyova.tasks.common.data.model

inline class TaskId(val id: Int)