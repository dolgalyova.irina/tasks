package com.dolgalyova.tasks.common.arch

interface OnBackPressListener {
    fun onBackPress(): Boolean
}