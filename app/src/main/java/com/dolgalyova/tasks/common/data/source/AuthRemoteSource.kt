package com.dolgalyova.tasks.common.data.source

import com.dolgalyova.tasks.common.data.model.Credentials
import com.dolgalyova.tasks.common.data.model.JwtToken
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthRemoteSource {

    @POST("auth")
    fun login(@Body credentials: Credentials): Single<JwtToken>

    @POST("users")
    fun register(@Body credentials: Credentials): Single<JwtToken>
}