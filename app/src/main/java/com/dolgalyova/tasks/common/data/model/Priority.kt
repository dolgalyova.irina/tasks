package com.dolgalyova.tasks.common.data.model

import com.google.gson.annotations.SerializedName

enum class Priority {
    @SerializedName("Low")
    Low,
    @SerializedName("Normal")
    Normal,
    @SerializedName("High")
    High
}