package com.dolgalyova.tasks.common.data.model

data class JwtToken(val token: String)