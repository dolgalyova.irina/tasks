package com.dolgalyova.tasks.common.data.worker

import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.*
import com.dolgalyova.tasks.BuildConfig
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.common.data.model.Priority
import com.dolgalyova.tasks.common.data.model.TaskInfo
import java.util.concurrent.TimeUnit

class ReminderWorker(context: Context, params: WorkerParameters) : Worker(context, params) {

    companion object {
        private const val EXTRA_TITLE = "${BuildConfig.APPLICATION_ID}.EXTRA_TITLE"
        private const val EXTRA_PRIORITY = "${BuildConfig.APPLICATION_ID}.EXTRA_PRIORITY"

        fun schedule(task: TaskInfo) {
            val extras = Data.Builder()
                    .putString(EXTRA_TITLE, task.title)
                    .putInt(EXTRA_PRIORITY, task.priority?.ordinal ?: -1)
                    .build()
            val diff = task.dueDate.time - System.currentTimeMillis()
            val params = OneTimeWorkRequestBuilder<ReminderWorker>()
                    .setInitialDelay(diff, TimeUnit.MILLISECONDS)
                    .addTag(task.id.toString())
                    .setInputData(extras)
                    .build()

            WorkManager.getInstance()
                    .beginUniqueWork(task.id.toString(), ExistingWorkPolicy.REPLACE, params)
                    .enqueue()
        }

        fun cancel(task: TaskInfo) {
            WorkManager.getInstance().cancelAllWorkByTag(task.id.toString())
        }
    }

    override fun doWork(): Result {
        val title = inputData.getString(EXTRA_TITLE).orEmpty()
        val priorityOrdinal = inputData.getInt(EXTRA_PRIORITY, -1)
        val priority = if (priorityOrdinal >= 0) Priority.values()[priorityOrdinal] else null

        val content = if (priority == null) title
        else "$title\n${priority.name}"
        val id = tags.firstOrNull().orEmpty().hashCode()

        val notification = NotificationCompat.Builder(applicationContext, BuildConfig.APPLICATION_ID)
                .setContentTitle(applicationContext.getString(R.string.app_name))
                .setContentText(content)
                .build()
        NotificationManagerCompat.from(applicationContext).notify(id, notification)

        return Result.success()
    }
}