package com.dolgalyova.tasks.common.view

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import java.util.*

object DatePicker {

    fun create(context: Context, onDateSelected: (Date) -> Unit): Dialog {
        val today = Calendar.getInstance()
        val todayDay = today[Calendar.DAY_OF_MONTH]
        val todayMonth = today[Calendar.MONTH]
        val todayYear = today[Calendar.YEAR]
        return DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            val calendar = Calendar.getInstance().apply {
                this[Calendar.DAY_OF_MONTH] = dayOfMonth
                this[Calendar.MONTH] = month
                this[Calendar.YEAR] = year
            }
            onDateSelected(calendar.time)
        }, todayYear, todayMonth, todayDay)
    }
}