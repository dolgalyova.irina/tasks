package com.dolgalyova.tasks.common.data.model

import java.util.*

data class TaskInfo(val id: Int,
                    val title: String,
                    val dueDate: Date,
                    val priority: Priority?,
                    val hasReminder: Boolean,
                    val isUploaded: Boolean)