package com.dolgalyova.tasks.common.data.source.db.dao

import androidx.room.*
import com.dolgalyova.tasks.common.data.source.db.entity.TaskEntity

@Dao
interface TaskDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun create(vararg tasks: TaskEntity)

    @Query("SELECT * FROM tasks")
    fun readAll(): List<TaskEntity>

    @Query("SELECT * FROM tasks WHERE id = :id")
    fun getById(id: Int): TaskEntity?

    @Query("SELECT * FROM tasks WHERE hasReminder = 1")
    fun getWithReminders(): List<TaskEntity>

    @Query("SELECT * FROM tasks WHERE isUploaded = 0")
    fun getNotUploaded(): List<TaskEntity>

    @Delete
    fun delete(vararg tasks: TaskEntity)
}