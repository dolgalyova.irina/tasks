package com.dolgalyova.tasks.common.data.source

interface AuthLocalStorage {
    var token: String?
}