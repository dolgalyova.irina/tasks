package com.dolgalyova.tasks.common.data.repository

import com.dolgalyova.tasks.common.data.model.SortCriteria
import com.dolgalyova.tasks.common.data.model.TaskInfo
import io.reactivex.Completable
import io.reactivex.Single

interface TaskRepository {

    fun create(task: TaskInfo): Single<TaskInfo>

    fun getById(id: Int): Single<TaskInfo>

    fun getAll(page: Int, sort: SortCriteria): Single<List<TaskInfo>>

    fun getTasksWithReminder(): Single<List<TaskInfo>>

    fun getNotUploadedTasks(): Single<List<TaskInfo>>

    fun update(task: TaskInfo): Completable

    fun delete(id: Int): Completable
}