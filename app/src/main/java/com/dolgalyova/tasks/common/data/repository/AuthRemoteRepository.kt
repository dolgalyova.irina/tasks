package com.dolgalyova.tasks.common.data.repository

import com.dolgalyova.tasks.common.data.model.Credentials
import com.dolgalyova.tasks.common.data.source.AuthLocalStorage
import com.dolgalyova.tasks.common.data.source.AuthRemoteSource
import io.reactivex.Completable
import io.reactivex.Single

class AuthRemoteRepository(private val remoteSource: AuthRemoteSource,
                           private val localSource: AuthLocalStorage) : AuthRepository {

    override fun login(credentials: Credentials): Completable {
        return remoteSource.login(credentials)
                .doOnSuccess { localSource.token = it.token }
                .toCompletable()
    }

    override fun register(credentials: Credentials): Completable {
        return remoteSource.register(credentials)
                .doOnSuccess { localSource.token = it.token }
                .toCompletable()
    }

    override fun isUserAuthorized(): Single<Boolean> {
        return Single.fromCallable { !localSource.token.isNullOrEmpty() }
    }
}