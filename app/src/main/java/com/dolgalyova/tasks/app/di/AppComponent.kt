package com.dolgalyova.tasks.app.di

import com.dolgalyova.tasks.app.TasksApp
import com.dolgalyova.tasks.common.data.worker.SyncTasksComponent
import com.dolgalyova.tasks.screens.host.di.MainComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(target: TasksApp)

    fun plusMain(): MainComponent.Builder
    fun plusSyncTasks(): SyncTasksComponent.Builder

    @Component.Builder
    interface Builder {
        fun module(module: AppModule): Builder
        @BindsInstance fun target(target: TasksApp): Builder
        fun build(): AppComponent
    }
}