package com.dolgalyova.tasks.app

import android.app.Application
import com.dolgalyova.tasks.BuildConfig
import com.dolgalyova.tasks.app.di.DaggerAppComponent
import com.dolgalyova.tasks.common.data.worker.SyncTasksComponent
import com.dolgalyova.tasks.common.data.worker.SyncTasksWorker
import com.dolgalyova.tasks.screens.host.MainActivity
import com.dolgalyova.tasks.screens.host.di.MainComponent
import com.dolgalyova.tasks.screens.host.di.MainModule
import com.dolgalyova.tasks.screens.host.di.MainNavigationModule
import timber.log.Timber

class TasksApp : Application(), MainComponent.ComponentProvider, SyncTasksComponent.ComponentProvider {

    val component by lazy {
        DaggerAppComponent.builder()
                .target(this)
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        SyncTasksWorker.start()
    }

    override fun provideMainComponent(target: MainActivity): MainComponent {
        return component.plusMain()
                .module(MainModule())
                .navigationModule(MainNavigationModule())
                .target(target)
                .build()
    }

    override fun provideSyncTasksComponent(): SyncTasksComponent {
        return component.plusSyncTasks().build()
    }
}