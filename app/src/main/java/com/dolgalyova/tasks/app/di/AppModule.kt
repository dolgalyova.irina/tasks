package com.dolgalyova.tasks.app.di

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import com.dolgalyova.tasks.app.TasksApp
import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.AuthInterceptor
import com.dolgalyova.tasks.common.data.repository.AuthRemoteRepository
import com.dolgalyova.tasks.common.data.repository.AuthRepository
import com.dolgalyova.tasks.common.data.repository.TaskLocalRestRepository
import com.dolgalyova.tasks.common.data.repository.TaskRepository
import com.dolgalyova.tasks.common.data.source.*
import com.dolgalyova.tasks.common.data.source.db.AppDatabase
import com.dolgalyova.tasks.common.util.createApiService
import com.dolgalyova.tasks.screens.host.router.AppNavigationHost
import com.dolgalyova.tasks.screens.host.router.NavigationHost
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun appContext(application: TasksApp): Context = application.applicationContext

    @Provides
    @Singleton
    fun navigationHost(): NavigationHost = AppNavigationHost()

    @Provides
    @Singleton
    fun client(authLocalStorage: AuthLocalStorage): OkHttpClient {
        return OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(AuthInterceptor(authLocalStorage))
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()
    }

    @Provides
    @Singleton
    fun remoteAuthSource(client: OkHttpClient) =
            createApiService(AuthRemoteSource::class.java, client)

    @Provides
    @Singleton
    fun preferences(application: TasksApp) =
            PreferenceManager.getDefaultSharedPreferences(application)

    @Provides
    @Singleton
    fun localAuthSource(preferences: SharedPreferences): AuthLocalStorage =
            AuthPreferencesStorage(preferences)

    @Provides
    @Singleton
    fun authRepository(
            remoteAuthSource: AuthRemoteSource,
            localAuthSource: AuthLocalStorage
    ): AuthRepository =
            AuthRemoteRepository(remoteAuthSource, localAuthSource)

    @Provides
    @Singleton
    fun tasksDatabase(application: TasksApp): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, "db-tasks")
                .allowMainThreadQueries()
                .build()
    }

    @Provides
    @Singleton
    fun taskLocalSource(database: AppDatabase): TaskLocalSource = TaskRoomSource(database.taskDao)

    @Provides
    @Singleton
    fun taskRemoteSource(client: OkHttpClient) =
            createApiService(TaskRemoteSource::class.java, client)

    @Provides
    @Singleton
    fun tasksRepository(
            remoteSource: TaskRemoteSource,
            localSource: TaskLocalSource): TaskRepository =
            TaskLocalRestRepository(remoteSource, localSource)

    @Provides
    @Singleton
    fun workers() = RxWorkers(Schedulers.io(), AndroidSchedulers.mainThread())
}