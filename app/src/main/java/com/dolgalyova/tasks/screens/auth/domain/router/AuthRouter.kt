package com.dolgalyova.tasks.screens.auth.domain.router

interface AuthRouter{
    fun openNextFlow()
    fun exit()
}