package com.dolgalyova.tasks.screens.reminders.router

import com.dolgalyova.tasks.common.data.model.TaskId
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowNavigator

class RemindersFragmentRouter(private val navigator: MainFlowNavigator) : RemindersRouter {

    override fun openDetails(id: Int) = navigator.setScreen(MainFlowNavigator.Screen.TaskDetails, TaskId(id))

    override fun exit() = navigator.goBack()
}