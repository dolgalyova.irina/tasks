package com.dolgalyova.tasks.screens.task_list.domain

import com.dolgalyova.tasks.common.arch.Interactor
import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.model.SortCriteria
import com.dolgalyova.tasks.common.data.model.TaskInfo
import com.dolgalyova.tasks.common.data.repository.TaskRepository
import com.dolgalyova.tasks.common.util.composeWith

class TaskListInteractor(private val repository: TaskRepository,
                         private val workers: RxWorkers) : Interactor() {

    fun getTaskList(page: Int, sortCriteria: SortCriteria, onSuccess: (List<TaskInfo>) -> Unit, onError: (Throwable) -> Unit) {
        addSubscription(repository.getAll(page, sortCriteria)
                .composeWith(workers)
                .subscribe(onSuccess, onError))
    }
}