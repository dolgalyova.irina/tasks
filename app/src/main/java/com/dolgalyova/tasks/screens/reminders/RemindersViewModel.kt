package com.dolgalyova.tasks.screens.reminders

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dolgalyova.tasks.common.arch.SingleLiveEvent
import com.dolgalyova.tasks.screens.reminders.domain.RemindersInteractor
import com.dolgalyova.tasks.screens.reminders.domain.model.Reminder
import com.dolgalyova.tasks.screens.reminders.router.RemindersRouter
import timber.log.Timber
import javax.inject.Inject

class RemindersViewModel(injector: (RemindersViewModel) -> Unit) : ViewModel() {
    private val reminders = MutableLiveData<List<Reminder>>()
    val observeReminders: LiveData<List<Reminder>>
        get() = reminders
    @Inject lateinit var interactor: RemindersInteractor
    @Inject lateinit var router: RemindersRouter

    private val generalError = SingleLiveEvent<Any>()
    val observeGeneralErrors: LiveData<Any>
        get() = generalError

    init {
        injector(this)
        onLoadMore()
    }

    fun onItemClick(item: Reminder) = router.openDetails(item.id)

    fun onLoadMore() {
        interactor.getReminders({ reminders.value = it }, ::onError)
    }

    fun onBackClick() = router.exit()

    private fun onError(error: Throwable) {
        generalError.call()
        Timber.e(error)
    }
}