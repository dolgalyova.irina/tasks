package com.dolgalyova.tasks.screens.reminders.di

import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.repository.TaskRepository
import com.dolgalyova.tasks.common.di.ScreenScope
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowNavigator
import com.dolgalyova.tasks.screens.reminders.domain.RemindersInteractor
import com.dolgalyova.tasks.screens.reminders.router.RemindersFragmentRouter
import com.dolgalyova.tasks.screens.reminders.router.RemindersRouter
import dagger.Module
import dagger.Provides

@Module
class RemindersModule {

    @Provides
    @ScreenScope
    fun router(navigator: MainFlowNavigator): RemindersRouter = RemindersFragmentRouter(navigator)

    @Provides
    @ScreenScope
    fun interactor(repository: TaskRepository, workers: RxWorkers) =
            RemindersInteractor(repository, workers)
}