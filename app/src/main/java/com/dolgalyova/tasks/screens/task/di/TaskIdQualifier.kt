package com.dolgalyova.tasks.screens.task.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class TaskIdQualifier