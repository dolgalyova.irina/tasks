package com.dolgalyova.tasks.screens.task.router

import com.dolgalyova.tasks.common.data.model.TaskId
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowNavigator

class TaskDetailsFragmentRouter(private val navigator: MainFlowNavigator) : TaskDetailsRouter {

    override fun openEdit(id: Int) = navigator.setScreen(MainFlowNavigator.Screen.EditTask, TaskId(id))

    override fun exit() = navigator.goBack()
}