package com.dolgalyova.tasks.screens.edit_task.domain

import com.dolgalyova.tasks.common.arch.Interactor
import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.model.Priority
import com.dolgalyova.tasks.common.data.model.TaskId
import com.dolgalyova.tasks.common.data.model.TaskInfo
import com.dolgalyova.tasks.common.data.repository.TaskRepository
import com.dolgalyova.tasks.common.data.worker.ReminderWorker
import com.dolgalyova.tasks.common.data.worker.SyncTasksWorker
import com.dolgalyova.tasks.common.util.composeWith
import com.dolgalyova.tasks.common.util.isNoInternetError
import com.dolgalyova.tasks.common.util.parseReadableDate
import com.dolgalyova.tasks.screens.task.domain.exception.InvalidDueDateException
import io.reactivex.Completable
import java.util.*

class EditTaskInteractor(private val repository: TaskRepository,
                         private val taskId: TaskId,
                         private val workers: RxWorkers) : Interactor() {

    fun getTask(onSuccess: (TaskInfo) -> Unit, onError: (Throwable) -> Unit) {
        if (taskId.id != -1) {
            addSubscription(repository.getById(taskId.id)
                    .composeWith(workers)
                    .subscribe(onSuccess, onError))
        }
    }

    fun saveTask(title: String, priority: Priority?, dueBy: String, hasReminder: Boolean,
                 onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        if (title.isBlank()) onError(InvalidTitleException())

        val date = dueBy.parseReadableDate()?.time
        if (date == null) {
            onError(InvalidDueDateException())
        } else {
            val taskId = if (taskId.id >= 0) taskId.id else UUID.randomUUID().hashCode()
            val task = TaskInfo(taskId, title, Date(date), priority, hasReminder, false)
            val request = if (this.taskId.id >= 0) {
                repository.update(task)
            } else {
                repository.create(task).toCompletable()
            }
            addSubscription(request
                    .onErrorResumeNext { error ->
                        if (error.isNoInternetError()) {
                            SyncTasksWorker.start()
                            Completable.complete()
                        } else Completable.error(error)
                    }
                    .doOnComplete {
                        if (hasReminder) ReminderWorker.schedule(task)
                        else ReminderWorker.cancel(task)
                    }
                    .composeWith(workers)
                    .subscribe(onSuccess, onError))
        }
    }
}

class InvalidTitleException : IllegalArgumentException()