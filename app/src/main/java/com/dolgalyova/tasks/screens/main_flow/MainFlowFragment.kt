package com.dolgalyova.tasks.screens.main_flow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.common.arch.OnBackPressListener
import com.dolgalyova.tasks.screens.edit_task.di.EditTaskComponent
import com.dolgalyova.tasks.screens.edit_task.di.EditTaskModule
import com.dolgalyova.tasks.screens.main_flow.di.MainFlowComponent
import com.dolgalyova.tasks.screens.reminders.di.RemindersComponent
import com.dolgalyova.tasks.screens.reminders.di.RemindersModule
import com.dolgalyova.tasks.screens.task.di.TaskDetailsComponent
import com.dolgalyova.tasks.screens.task.di.TaskDetailsModule
import com.dolgalyova.tasks.screens.task_list.di.TaskListComponent
import com.dolgalyova.tasks.screens.task_list.di.TaskListModule

class MainFlowFragment : Fragment(), TaskListComponent.ComponentProvider, TaskDetailsComponent.ComponentProvider,
        EditTaskComponent.ComponentProvider, RemindersComponent.ComponentProvider, OnBackPressListener {

    companion object {
        fun create() = MainFlowFragment()
    }

    private val component by lazy {
        (activity as MainFlowComponent.ComponentProvider).provideMainFlowComponent(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_flow_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewModelProviders.of(this, MainFlowViewModelFactory(component)).get(MainFlowViewModel::class.java)
    }

    override fun onBackPress(): Boolean {
        val topFragment = childFragmentManager.fragments.firstOrNull { it is OnBackPressListener }
        return if (topFragment?.isVisible == true) {
            (topFragment as? OnBackPressListener)?.onBackPress() ?: false
        } else false
    }

    override fun provideTaskListComponent(): TaskListComponent {
        return component.plusTaskList()
                .module(TaskListModule())
                .build()
    }

    override fun provideTaskDetailsComponent(taskId: Int): TaskDetailsComponent {
        return component.plusTaskDetails()
                .module(TaskDetailsModule())
                .taskId(taskId)
                .build()
    }

    override fun provideEditTaskComponent(taskId: Int): EditTaskComponent {
        return component.plusEditTask()
                .taskId(taskId)
                .module(EditTaskModule())
                .build()
    }

    override fun provideRemindersComponent(): RemindersComponent {
        return component.plusReminders()
                .module(RemindersModule())
                .build()
    }
}