package com.dolgalyova.tasks.screens.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dolgalyova.tasks.common.arch.SingleLiveEvent
import com.dolgalyova.tasks.screens.auth.domain.AuthInteractor
import com.dolgalyova.tasks.screens.auth.domain.InvalidEmailException
import com.dolgalyova.tasks.screens.auth.domain.InvalidPasswordException
import com.dolgalyova.tasks.screens.auth.domain.model.Email
import com.dolgalyova.tasks.screens.auth.domain.model.Password
import com.dolgalyova.tasks.screens.auth.domain.router.AuthRouter
import timber.log.Timber
import javax.inject.Inject

class AuthViewModel(injector: (AuthViewModel) -> Unit) : ViewModel() {
    @Inject lateinit var interactor: AuthInteractor
    @Inject lateinit var router: AuthRouter

    init {
        injector(this)
    }

    private val passwordError = SingleLiveEvent<Any>()
    val observePasswordErrors: LiveData<Any>
        get() = passwordError
    private val authError = SingleLiveEvent<Any>()
    val observeAuthErrors: LiveData<Any>
        get() = authError
    private val invalidEmailError = SingleLiveEvent<Any>()
    val observeInvalidEmailErrors: LiveData<Any>
        get() = invalidEmailError

    fun onLoginClick(emailValue: String, passwordValue: String, isRegister: Boolean) {
        val email = Email(emailValue)
        val password = Password(passwordValue)

        if (isRegister) interactor.register(email, password, ::onAuthSuccess, ::onError)
        else interactor.login(email, password, ::onAuthSuccess, ::onError)
    }

    private fun onAuthSuccess() = router.openNextFlow()

    private fun onError(error: Throwable) = when (error) {
        is InvalidEmailException -> invalidEmailError.call()
        is InvalidPasswordException -> passwordError.call()
        else -> authError.call()
    }.also { Timber.e(error) }
}