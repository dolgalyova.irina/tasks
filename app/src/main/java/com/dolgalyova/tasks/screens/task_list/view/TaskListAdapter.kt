package com.dolgalyova.tasks.screens.task_list.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.common.data.model.Priority
import com.dolgalyova.tasks.common.data.model.TaskInfo
import com.dolgalyova.tasks.common.util.toReadableDate
import com.dolgalyova.tasks.screens.task_list.TaskListItem
import kotlinx.android.synthetic.main.item_task_list_task.view.*

class TaskListAdapter(private val onItemClick: (TaskInfo) -> Unit) :
        ListAdapter<TaskListItem, RecyclerView.ViewHolder>(TaskListItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.item_task_list_task -> TaskHolder(itemView, onItemClick)
            else -> ProgressHolder(itemView)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        if (item is TaskListItem.Info) {
            val task = item.item
            (holder as? TaskHolder)?.task = task
            with(holder.itemView) {
                title.text = item.item.title
                date.text = item.item.dueDate.toReadableDate()
                priorityIcon.isVisible = true

                when (task.priority) {
                    Priority.Low -> {
                        priorityIcon.rotation = 90f
                        priority.text = resources.getString(R.string.label_low)
                    }
                    Priority.Normal -> {
                        priorityIcon.visibility = View.INVISIBLE
                        priority.text = resources.getString(R.string.label_medium)
                    }
                    Priority.High -> {
                        priorityIcon.rotation = 270f
                        priority.text = resources.getString(R.string.label_high)
                    }
                    null -> {
                        priorityIcon.isVisible = false
                        priority.isVisible = false
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return when (item) {
            is TaskListItem.Info -> R.layout.item_task_list_task
            is TaskListItem.InitialProgress -> R.layout.item_task_list_initial_progress
            is TaskListItem.LoadNextProgress -> R.layout.item_task_list_load_more_progress
        }
    }

    class TaskHolder(itemView: View, onItemClick: (TaskInfo) -> Unit) : RecyclerView.ViewHolder(itemView) {
        internal var task: TaskInfo? = null

        init {
            itemView.setOnClickListener { _ -> task?.let { onItemClick(it) } }
        }
    }

    class ProgressHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}