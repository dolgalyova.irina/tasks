package com.dolgalyova.tasks.screens.task_list

import com.dolgalyova.tasks.common.data.model.TaskInfo

sealed class TaskListItem {
    object InitialProgress : TaskListItem()
    object LoadNextProgress : TaskListItem()
    class Info(val item: TaskInfo) : TaskListItem()
}