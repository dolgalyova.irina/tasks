package com.dolgalyova.tasks.screens.auth.domain

import com.dolgalyova.tasks.common.arch.Interactor
import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.model.Credentials
import com.dolgalyova.tasks.common.data.repository.AuthRepository
import com.dolgalyova.tasks.common.util.composeWith
import com.dolgalyova.tasks.screens.auth.domain.model.Email
import com.dolgalyova.tasks.screens.auth.domain.model.Password

class AuthInteractor(private val repository: AuthRepository,
                     private val workers: RxWorkers) : Interactor() {

    fun login(login: Email, password: Password, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        if (!isEmailValid(login)) {
            onError(InvalidEmailException())
        } else if (password.data.isBlank()) {
            onError(InvalidPasswordException())
        } else {
            addSubscription(repository.login(Credentials(login.data, password.data))
                    .composeWith(workers)
                    .subscribe(onSuccess, onError))
        }
    }

    fun register(login: Email, password: Password, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        if (!isEmailValid(login)) {
            onError(InvalidEmailException())
        } else {
            addSubscription(repository.register(Credentials(login.data, password.data))
                    .composeWith(workers)
                    .subscribe(onSuccess, onError))
        }
    }
}

private fun isEmailValid(login: Email): Boolean = android.util.Patterns.EMAIL_ADDRESS.matcher(login.data).matches()

class InvalidEmailException : IllegalArgumentException()
class InvalidPasswordException : IllegalArgumentException()