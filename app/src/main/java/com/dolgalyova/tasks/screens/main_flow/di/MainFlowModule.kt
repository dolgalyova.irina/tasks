package com.dolgalyova.tasks.screens.main_flow.di

import com.dolgalyova.tasks.common.di.FlowScope
import com.dolgalyova.tasks.screens.main_flow.MainFlowFragment
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowNavigator
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowRouter
import dagger.Module
import dagger.Provides

@Module
class MainFlowModule {

    @Provides
    @FlowScope
    fun navigator(router: MainFlowRouter, host: MainFlowFragment) = MainFlowNavigator(host, router)
}