package com.dolgalyova.tasks.screens.task

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.tasks.BuildConfig
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.common.arch.OnBackPressListener
import com.dolgalyova.tasks.common.data.model.Priority.*
import com.dolgalyova.tasks.common.data.model.TaskId
import com.dolgalyova.tasks.common.data.model.TaskInfo
import com.dolgalyova.tasks.common.util.toReadableDate
import com.dolgalyova.tasks.screens.task.di.TaskDetailsComponent
import kotlinx.android.synthetic.main.fragment_task_details.*

class TaskDetailsFragment : Fragment(), OnBackPressListener {
    companion object {
        private const val EXTRA_TASK_ID = "${BuildConfig.APPLICATION_ID}.EXTRA_TASK_ID"

        fun create(id: TaskId): TaskDetailsFragment {
            val args = Bundle().apply { putInt(EXTRA_TASK_ID, id.id) }
            return TaskDetailsFragment().apply { arguments = args }
        }
    }

    private val component by lazy {
        val taskId = arguments?.getInt(EXTRA_TASK_ID, -1) ?: -1
        (parentFragment as TaskDetailsComponent.ComponentProvider).provideTaskDetailsComponent(taskId)
    }
    private val viewModel by lazy {
        ViewModelProviders.of(this, TaskDetailsViewModelFactory(component)).get(TaskDetailsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_task_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        viewModel.itemObservable.observe(viewLifecycleOwner, Observer { setData(it) })
        viewModel.observeGeneralErrors.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, R.string.error_general, Toast.LENGTH_SHORT).show()
        })
    }

    override fun onStart() {
        super.onStart()
        viewModel.doOnStart()
    }

    override fun onBackPress(): Boolean {
        viewModel.onBackClick()
        return true
    }

    private fun initViews() {
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener { viewModel.onBackClick() }
        toolbar.inflateMenu(R.menu.menu_edit)
        toolbar.setOnMenuItemClickListener {
            if (it.itemId == R.id.action_edit) viewModel.onEditClick()
            true
        }
        delete.setOnClickListener { viewModel.onDeleteClick() }
    }

    private fun setData(task: TaskInfo) {
        toolbar.title = task.title
        dueDate.text = task.dueDate.toReadableDate()
        priorityIcon.isVisible = true
        priority.isVisible = true
        priorityLabel.isVisible = true

        val reminderText = if (task.hasReminder) R.string.label_enabled
        else R.string.label_disabled
        reminder.text = getString(reminderText)
        when (task.priority) {
            Low -> {
                priorityIcon.rotation = 90f
                priority.text = getString(R.string.label_low)
            }
            Normal -> {
                priorityIcon.isVisible = false
                priority.text = getString(R.string.label_medium)
            }
            High -> {
                priorityIcon.rotation = 270f
                priority.text = getString(R.string.label_high)
            }
            null -> {
                priorityLabel.isVisible = false
                priorityIcon.isVisible = false
                priority.isVisible = false
            }
        }
    }
}