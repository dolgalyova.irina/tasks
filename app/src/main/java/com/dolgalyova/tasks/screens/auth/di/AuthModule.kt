package com.dolgalyova.tasks.screens.auth.di

import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.repository.AuthRepository
import com.dolgalyova.tasks.common.di.FlowScope
import com.dolgalyova.tasks.screens.auth.domain.AuthInteractor
import com.dolgalyova.tasks.screens.auth.domain.router.AuthFragmentRouter
import com.dolgalyova.tasks.screens.auth.domain.router.AuthRouter
import com.dolgalyova.tasks.screens.host.router.AppNavigator
import dagger.Module
import dagger.Provides

@Module
class AuthModule {
    @Provides
    @FlowScope
    fun authRouter(navigator: AppNavigator): AuthRouter = AuthFragmentRouter(navigator)

    @Provides
    @FlowScope
    fun interactor(repository: AuthRepository, workers: RxWorkers) =
            AuthInteractor(repository, workers)
}