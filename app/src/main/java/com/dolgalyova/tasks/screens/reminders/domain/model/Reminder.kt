package com.dolgalyova.tasks.screens.reminders.domain.model

import java.util.*

data class Reminder(val id: Int,
                    val title: String,
                    val date: Date)