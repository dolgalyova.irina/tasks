package com.dolgalyova.tasks.screens.task

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dolgalyova.tasks.common.arch.SingleLiveEvent
import com.dolgalyova.tasks.common.data.exception.ItemDoesntExistException
import com.dolgalyova.tasks.common.data.model.TaskInfo
import com.dolgalyova.tasks.screens.task.domain.TaskDetailsInteractor
import com.dolgalyova.tasks.screens.task.router.TaskDetailsRouter
import timber.log.Timber
import javax.inject.Inject

class TaskDetailsViewModel(injector: (TaskDetailsViewModel) -> Unit) : ViewModel() {
    @Inject lateinit var interactor: TaskDetailsInteractor
    @Inject lateinit var router: TaskDetailsRouter

    private val item = MutableLiveData<TaskInfo>()
    val itemObservable: LiveData<TaskInfo>
        get() = item

    private val generalError = SingleLiveEvent<Any>()
    val observeGeneralErrors: LiveData<Any>
        get() = generalError


    init {
        injector(this)
    }

    fun doOnStart() {
        interactor.getTask({ item.value = it }, ::onError)
    }

    fun onDeleteClick() {
        interactor.deleteTask(::onTaskDeleted, ::onError)
    }

    fun onEditClick() {
        router.openEdit(item.value?.id ?: -1)
    }

    fun onBackClick() = router.exit()

    private fun onTaskDeleted() = router.exit()

    private fun onError(error: Throwable) = when (error) {
        is ItemDoesntExistException -> {
            // do nothing
        }
        else -> generalError.call()
    }.also { Timber.e(error) }
}