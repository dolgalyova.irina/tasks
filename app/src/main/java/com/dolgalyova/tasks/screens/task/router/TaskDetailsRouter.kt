package com.dolgalyova.tasks.screens.task.router

interface TaskDetailsRouter {
    fun openEdit(id: Int)
    fun exit()
}