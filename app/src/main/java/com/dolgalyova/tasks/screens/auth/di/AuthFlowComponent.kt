package com.dolgalyova.tasks.screens.auth.di

import com.dolgalyova.tasks.common.di.FlowScope
import com.dolgalyova.tasks.screens.auth.AuthViewModel
import dagger.Subcomponent

@FlowScope
@Subcomponent(modules = [AuthModule::class])
interface AuthFlowComponent {

    fun inject(target: AuthViewModel)

    @Subcomponent.Builder
    interface Builder {
        fun module(module: AuthModule): Builder
        fun build(): AuthFlowComponent
    }

    interface ComponentProvider {
        fun provideAuthComponent(): AuthFlowComponent
    }
}