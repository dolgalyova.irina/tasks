package com.dolgalyova.tasks.screens.host.router

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.common.util.canHandleNavigation
import com.dolgalyova.tasks.common.util.clearBackStack
import com.dolgalyova.tasks.screens.auth.AuthFragment
import com.dolgalyova.tasks.screens.main_flow.MainFlowFragment

class AppNavigator(private val navigationHost: NavigationHost) {
    private var pendingScreen: Pair<Flow, Any?>? = null
    private val host: AppCompatActivity?
        get() = navigationHost.getHost()

    init {
        navigationHost.onAttachHost = ::dispatchPendingTransaction
    }

    fun setFlow(flow: Flow, data: Any? = null) {
        if (host?.canHandleNavigation() == true) {
            val fragment: Fragment? = when (flow) {
                Flow.Auth -> AuthFragment.create()
                Flow.Main -> MainFlowFragment.create()
            }
            val addToBackStack = false
            fragment?.let { setFragment(it, addToBackStack) }
        } else {
            pendingScreen = flow to data
            attachObserver()
        }
    }

    fun goBack() {
        val host = this.host
        if (host?.canHandleNavigation() == true) {
            val fm = host.supportFragmentManager
            if (fm.backStackEntryCount > 0) fm.popBackStack()
            else host.finish()
        }
    }

    private fun setFragment(child: Fragment, addToBackStack: Boolean) {
        val host = this.host
        if (host?.canHandleNavigation() == true) {
            val fm = host.supportFragmentManager
            val transaction = fm
                    .beginTransaction()
                    .replace(R.id.flowContainer, child, child::class.java.simpleName)

            if (addToBackStack) transaction.addToBackStack(null)
            else fm.clearBackStack()

            transaction.commit()
        }
    }

    private fun attachObserver() {
        val host = this.host
        host?.lifecycle?.addObserver(object : LifecycleObserver {

            @OnLifecycleEvent(Lifecycle.Event.ON_START)
            fun dispatchPendingNavigation() {
                dispatchPendingTransaction()
                host.lifecycle.removeObserver(this)
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun clearObserver() = host.lifecycle.removeObserver(this)
        })
    }

    private fun dispatchPendingTransaction() {
        val screenData = pendingScreen
        pendingScreen = null
        screenData?.let { setFlow(it.first, it.second) }
    }
}

enum class Flow {
    Auth, Main
}