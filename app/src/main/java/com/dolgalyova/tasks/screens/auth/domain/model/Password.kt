package com.dolgalyova.tasks.screens.auth.domain.model

inline class Password(val data: String)