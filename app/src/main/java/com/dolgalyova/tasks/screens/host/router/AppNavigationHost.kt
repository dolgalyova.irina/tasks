package com.dolgalyova.tasks.screens.host.router

import androidx.appcompat.app.AppCompatActivity

class AppNavigationHost : NavigationHost {
    override var onAttachHost: () -> Unit = {}
    private var host: AppCompatActivity? = null

    override fun attachHost(activity: AppCompatActivity) {
        this.host = activity
        onAttachHost()
    }

    override fun detachHost() {
        this.host = null
    }

    override fun getHost(): AppCompatActivity? = this.host
}