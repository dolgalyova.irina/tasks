package com.dolgalyova.tasks.screens.task

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.tasks.screens.task.di.TaskDetailsComponent

class TaskDetailsViewModelFactory(private val component: TaskDetailsComponent) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        TaskDetailsViewModel { component.inject(it) } as T
}
