package com.dolgalyova.tasks.screens.edit_task.di

import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.model.TaskId
import com.dolgalyova.tasks.common.data.repository.TaskRepository
import com.dolgalyova.tasks.common.di.ScreenScope
import com.dolgalyova.tasks.screens.edit_task.domain.EditTaskInteractor
import com.dolgalyova.tasks.screens.edit_task.router.EditTaskFragmentRouter
import com.dolgalyova.tasks.screens.edit_task.router.EditTaskRouter
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowNavigator
import com.dolgalyova.tasks.screens.task.di.TaskIdQualifier
import dagger.Module
import dagger.Provides

@Module
class EditTaskModule {

    @Provides
    @ScreenScope
    fun router(navigator: MainFlowNavigator): EditTaskRouter = EditTaskFragmentRouter(navigator)

    @Provides
    @ScreenScope
    fun interactor(repository: TaskRepository,
                   @TaskIdQualifier taskId: Int,
                   workers: RxWorkers) =
            EditTaskInteractor(repository, TaskId(taskId), workers)
}