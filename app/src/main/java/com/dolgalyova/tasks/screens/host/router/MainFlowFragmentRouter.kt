package com.dolgalyova.tasks.screens.host.router

import com.dolgalyova.tasks.screens.main_flow.router.MainFlowRouter

class MainFlowFragmentRouter(private val navigator: AppNavigator) : MainFlowRouter {

    override fun goBack() = navigator.goBack()
}