package com.dolgalyova.tasks.screens.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.screens.auth.di.AuthFlowComponent
import kotlinx.android.synthetic.main.fragment_auth.*

class AuthFragment : Fragment() {

    companion object {
        fun create(): AuthFragment {
            return AuthFragment()
        }
    }

    private val component by lazy {
        (activity as AuthFlowComponent.ComponentProvider).provideAuthComponent()
    }
    private val viewModel by lazy {
        ViewModelProviders.of(this, AuthViewModelFactory(component)).get(AuthViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_auth, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        viewModel.observePasswordErrors.observe(viewLifecycleOwner, Observer {
            hideProgress()
            Toast.makeText(context, getString(R.string.error_password), Toast.LENGTH_SHORT).show()
        })
        viewModel.observeAuthErrors.observe(viewLifecycleOwner, Observer {
            hideProgress()
            Toast.makeText(context, getString(R.string.error_auth), Toast.LENGTH_SHORT).show()
        })
        viewModel.observeInvalidEmailErrors.observe(viewLifecycleOwner, Observer {
            showError()
            hideProgress()
        })
    }

    private fun initViews() {
        login.setOnClickListener {
            showProgress()
            viewModel.onLoginClick(email.text.toString(),
                    password.text.toString(), signSwitch.isChecked)
        }
    }

    private fun showProgress() {
        progress.isVisible = true
        login.visibility = View.INVISIBLE
    }

    private fun hideProgress() {
        progress.isVisible = false
        login.isVisible = true
    }
    private fun showError() {
        Toast.makeText(context, getString(R.string.error_invalid_email), Toast.LENGTH_SHORT).show()
    }
}