package com.dolgalyova.tasks.screens.task_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.common.data.model.SortField
import com.dolgalyova.tasks.common.data.model.SortType
import com.dolgalyova.tasks.common.util.setLoadMoreListener
import com.dolgalyova.tasks.common.util.showGeneralError
import com.dolgalyova.tasks.screens.task_list.di.TaskListComponent
import com.dolgalyova.tasks.screens.task_list.view.TaskListAdapter
import kotlinx.android.synthetic.main.fragment_task_list.*

class TaskListFragment : Fragment() {
    companion object {
        fun create(): TaskListFragment = TaskListFragment()
    }

    private val component by lazy {
        (parentFragment as TaskListComponent.ComponentProvider).provideTaskListComponent()
    }
    private val viewModel by lazy {
        ViewModelProviders.of(this, TaskListViewModelFactory(component))
                .get(TaskListViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_task_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()

        viewModel.itemsObservable.observe(viewLifecycleOwner, Observer {
            (taskList.adapter as TaskListAdapter).submitList(it)
            swipeRefresh.isRefreshing = false
        })
        viewModel.generalErrorObservable.observe(viewLifecycleOwner, Observer { showGeneralError() })
    }

    override fun onStart() {
        super.onStart()
        viewModel.onLoadMore()
    }

    private fun initViews() {
        bottomBar.inflateMenu(R.menu.menu_task_list)
        bottomBar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.actionOrderAscending -> viewModel.onSortOrderSelected(SortType.ASC)
                R.id.actionOrderDescending -> viewModel.onSortOrderSelected(SortType.DESC)
                R.id.actionSortDate -> viewModel.onSortFieldSelected(SortField.DATE)
                R.id.actionSortName -> viewModel.onSortFieldSelected(SortField.TITLE)
                R.id.actionSortPriority -> viewModel.onSortFieldSelected(SortField.PRIORITY)
                R.id.actionReminders -> viewModel.onOpenRemindersClick()
            }
            it.itemId != R.id.actionGroupSort && it.itemId != R.id.actionGroupOrder
        }
        taskList.layoutManager = LinearLayoutManager(context)
                .apply { orientation = RecyclerView.VERTICAL }
        taskList.itemAnimator = DefaultItemAnimator()
        taskList.adapter = TaskListAdapter(viewModel::onTaskClick)
        taskList.setLoadMoreListener(viewModel::onLoadMore)

        addScan.setOnClickListener { viewModel.onAddTaskClick() }
        swipeRefresh.setOnRefreshListener { viewModel.onLoadMore() }
    }
}