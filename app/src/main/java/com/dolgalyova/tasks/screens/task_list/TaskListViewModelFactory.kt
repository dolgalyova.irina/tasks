package com.dolgalyova.tasks.screens.task_list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.tasks.screens.task_list.di.TaskListComponent

class TaskListViewModelFactory(private val component: TaskListComponent) :
        ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            TaskListViewModel { component.inject(it) } as T
}