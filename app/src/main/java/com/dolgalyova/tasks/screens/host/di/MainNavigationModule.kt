package com.dolgalyova.tasks.screens.host.di

import com.dolgalyova.tasks.common.di.ActivityScope
import com.dolgalyova.tasks.screens.host.router.AppNavigator
import com.dolgalyova.tasks.screens.host.router.MainFlowFragmentRouter
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowRouter
import dagger.Module
import dagger.Provides

@Module
class MainNavigationModule {

    @Provides
    @ActivityScope
    fun mainFlowRouter(navigator: AppNavigator): MainFlowRouter = MainFlowFragmentRouter(navigator)

}