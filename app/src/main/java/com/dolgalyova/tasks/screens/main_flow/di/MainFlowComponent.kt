package com.dolgalyova.tasks.screens.main_flow.di

import com.dolgalyova.tasks.common.di.FlowScope
import com.dolgalyova.tasks.screens.edit_task.di.EditTaskComponent
import com.dolgalyova.tasks.screens.main_flow.MainFlowFragment
import com.dolgalyova.tasks.screens.main_flow.MainFlowViewModel
import com.dolgalyova.tasks.screens.reminders.di.RemindersComponent
import com.dolgalyova.tasks.screens.task.di.TaskDetailsComponent
import com.dolgalyova.tasks.screens.task_list.di.TaskListComponent
import dagger.BindsInstance
import dagger.Subcomponent

@FlowScope
@Subcomponent(modules = [MainFlowModule::class])
interface MainFlowComponent {

    fun inject(target: MainFlowViewModel)

    fun plusTaskList(): TaskListComponent.Builder
    fun plusTaskDetails(): TaskDetailsComponent.Builder
    fun plusEditTask(): EditTaskComponent.Builder
    fun plusReminders(): RemindersComponent.Builder

    @Subcomponent.Builder
    interface Builder {
        fun module(module: MainFlowModule): Builder
        @BindsInstance fun target(target: MainFlowFragment): Builder
        fun build(): MainFlowComponent
    }

    interface ComponentProvider {
        fun provideMainFlowComponent(target: MainFlowFragment): MainFlowComponent
    }
}