package com.dolgalyova.tasks.screens.reminders.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.common.util.toReadableDate
import com.dolgalyova.tasks.screens.reminders.domain.model.Reminder
import kotlinx.android.synthetic.main.item_reminder.view.*

class RemindersAdapter(private val onItemClick: (Reminder) -> Unit)
    : ListAdapter<Reminder, RemindersAdapter.ReminderHolder>(ReminderDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReminderHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ReminderHolder(inflater.inflate(viewType, parent, false), onItemClick)
    }

    override fun onBindViewHolder(holder: ReminderHolder, position: Int) {
        val item = getItem(position)
        holder.reminder = item
        with(holder.itemView) {
            title.text = item.title
            date.text = item.date.toReadableDate()
        }
    }

    override fun getItemViewType(position: Int): Int = R.layout.item_reminder

    class ReminderHolder(itemView: View, onItemClick: (Reminder) -> Unit) : RecyclerView.ViewHolder(itemView) {
        internal var reminder: Reminder? = null

        init {
            itemView.setOnClickListener { reminder?.let(onItemClick) }
        }
    }
}