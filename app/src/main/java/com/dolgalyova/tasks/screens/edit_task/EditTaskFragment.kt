package com.dolgalyova.tasks.screens.edit_task

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.tasks.BuildConfig
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.common.arch.OnBackPressListener
import com.dolgalyova.tasks.common.data.model.Priority.*
import com.dolgalyova.tasks.common.data.model.TaskId
import com.dolgalyova.tasks.common.data.model.TaskInfo
import com.dolgalyova.tasks.common.util.toReadableDate
import com.dolgalyova.tasks.common.view.DatePicker
import com.dolgalyova.tasks.screens.edit_task.di.EditTaskComponent
import kotlinx.android.synthetic.main.fragment_edit_task.*

class EditTaskFragment : Fragment(), OnBackPressListener {

    companion object {
        private const val EXTRA_TASK_ID = "${BuildConfig.APPLICATION_ID}.EXTRA_TASK_ID"

        fun create(id: TaskId?): EditTaskFragment {
            val args = Bundle().apply {
                if (id != null) putInt(EXTRA_TASK_ID, id.id)
            }
            return EditTaskFragment().apply { arguments = args }
        }
    }

    private val component by lazy {
        val taskId = arguments?.getInt(EXTRA_TASK_ID, -1) ?: -1
        (parentFragment as EditTaskComponent.ComponentProvider).provideEditTaskComponent(taskId)
    }
    private val viewModel by lazy {
        ViewModelProviders.of(this, EditTaskViewModelFactory(component))
                .get(EditTaskViewModel::class.java)
    }
    private var datePicker: Dialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_edit_task, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        viewModel.itemObservable.observe(viewLifecycleOwner, Observer { onTaskChanged(it) })
        viewModel.observeGeneralErrors.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, R.string.error_general, Toast.LENGTH_SHORT).show()
        })
        viewModel.observeInvalidDateErrors.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, R.string.error_invalid_date, Toast.LENGTH_SHORT).show()
        })
        viewModel.observeInvalidTitleErrors.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, R.string.error_invalid_title, Toast.LENGTH_SHORT).show()
        })
    }

    override fun onStop() {
        super.onStop()
        datePicker?.dismiss()
    }

    override fun onBackPress(): Boolean {
        viewModel.onBackClick()
        return true
    }

    private fun onTaskChanged(task: TaskInfo) {
        title.setText(task.title)
        val selected = when (task.priority) {
            Low -> R.id.priorityLow
            Normal -> R.id.prioritMedium
            High -> R.id.priorityHigh
            null -> null
        }
        selected?.let { priorityGroup.check(it) }
        dueDate.setText(task.dueDate.toReadableDate())
        reminder.isChecked = task.hasReminder
    }

    private fun initViews() {
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener { viewModel.onBackClick() }
        dueDate.setOnClickListener {
            datePicker = DatePicker.create(it.context) {
                dueDate.setText(it.toReadableDate())
                datePicker?.dismiss()
            }.also {
                (it as DatePickerDialog).datePicker.minDate = System.currentTimeMillis() - 1000
                it.show()
            }
        }
        done.setOnClickListener {
            val priority = when (priorityGroup.checkedChipId) {
                R.id.priorityLow -> Low
                R.id.prioritMedium -> Normal
                R.id.priorityHigh -> High
                else -> null
            }
            viewModel.onSaveClick(title.text.toString(), priority, dueDate.text.toString(), reminder.isChecked)
        }
    }
}