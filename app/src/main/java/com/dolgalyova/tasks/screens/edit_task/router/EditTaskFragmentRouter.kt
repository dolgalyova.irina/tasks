package com.dolgalyova.tasks.screens.edit_task.router

import com.dolgalyova.tasks.screens.main_flow.router.MainFlowNavigator

class EditTaskFragmentRouter(private val navigator: MainFlowNavigator) : EditTaskRouter {

    override fun exit() = navigator.goBack()
}