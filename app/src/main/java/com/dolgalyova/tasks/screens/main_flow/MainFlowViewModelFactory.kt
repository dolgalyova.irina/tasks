package com.dolgalyova.tasks.screens.main_flow

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.tasks.screens.main_flow.di.MainFlowComponent

class MainFlowViewModelFactory(private val component: MainFlowComponent) :
        ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            MainFlowViewModel { component.inject(it) } as T
}