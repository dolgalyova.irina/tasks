package com.dolgalyova.tasks.screens.reminders.di

import com.dolgalyova.tasks.common.di.ScreenScope
import com.dolgalyova.tasks.screens.reminders.RemindersViewModel
import dagger.Subcomponent

@ScreenScope
@Subcomponent(modules = [RemindersModule::class])
interface RemindersComponent {

    fun inject(target: RemindersViewModel)

    @Subcomponent.Builder
    interface Builder {
        fun module(module: RemindersModule): Builder
        fun build(): RemindersComponent
    }

    interface ComponentProvider {
        fun provideRemindersComponent(): RemindersComponent
    }
}