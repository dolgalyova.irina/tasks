package com.dolgalyova.tasks.screens.main_flow

import androidx.lifecycle.ViewModel
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowNavigator
import javax.inject.Inject

class MainFlowViewModel(injector: (MainFlowViewModel) -> Unit) : ViewModel() {
    @Inject lateinit var navigator: MainFlowNavigator

    init {
        injector(this)
        navigator.setScreen(MainFlowNavigator.Screen.TasksList)
    }
}