package com.dolgalyova.tasks.screens.auth

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.tasks.screens.auth.di.AuthFlowComponent

class AuthViewModelFactory(private val component: AuthFlowComponent) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            AuthViewModel { component.inject(it) } as T
}