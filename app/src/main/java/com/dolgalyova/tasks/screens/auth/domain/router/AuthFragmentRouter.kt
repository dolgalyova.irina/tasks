package com.dolgalyova.tasks.screens.auth.domain.router

import com.dolgalyova.tasks.screens.host.router.AppNavigator
import com.dolgalyova.tasks.screens.host.router.Flow

class AuthFragmentRouter(private val navigator: AppNavigator) : AuthRouter {
    override fun openNextFlow() = navigator.setFlow(Flow.Main)

    override fun exit() = navigator.goBack()
}