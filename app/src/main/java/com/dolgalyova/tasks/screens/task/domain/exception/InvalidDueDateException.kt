package com.dolgalyova.tasks.screens.task.domain.exception

class InvalidDueDateException : IllegalArgumentException()