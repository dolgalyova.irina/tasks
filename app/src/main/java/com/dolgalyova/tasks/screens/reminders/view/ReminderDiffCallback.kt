package com.dolgalyova.tasks.screens.reminders.view

import androidx.recyclerview.widget.DiffUtil
import com.dolgalyova.tasks.screens.reminders.domain.model.Reminder

class ReminderDiffCallback : DiffUtil.ItemCallback<Reminder>() {

    override fun areContentsTheSame(oldItem: Reminder, newItem: Reminder): Boolean = oldItem == newItem

    override fun areItemsTheSame(oldItem: Reminder, newItem: Reminder): Boolean = oldItem.id == newItem.id
}