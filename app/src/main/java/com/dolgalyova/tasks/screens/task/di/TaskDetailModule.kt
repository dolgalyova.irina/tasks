package com.dolgalyova.tasks.screens.task.di

import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.repository.TaskRepository
import com.dolgalyova.tasks.common.di.ScreenScope
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowNavigator
import com.dolgalyova.tasks.screens.task.domain.TaskDetailsInteractor
import com.dolgalyova.tasks.common.data.model.TaskId
import com.dolgalyova.tasks.screens.task.router.TaskDetailsFragmentRouter
import com.dolgalyova.tasks.screens.task.router.TaskDetailsRouter
import dagger.Module
import dagger.Provides

@Module
class TaskDetailsModule {

    @Provides
    @ScreenScope
    fun router(navigator: MainFlowNavigator): TaskDetailsRouter =
            TaskDetailsFragmentRouter(navigator)

    @Provides
    @ScreenScope
    fun interactor(repository: TaskRepository,
                   @TaskIdQualifier taskId: Int,
                   workers: RxWorkers): TaskDetailsInteractor {
        return TaskDetailsInteractor(TaskId(taskId), repository, workers)
    }
}