package com.dolgalyova.tasks.screens.host.di

import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.repository.AuthRepository
import com.dolgalyova.tasks.common.di.ActivityScope
import com.dolgalyova.tasks.screens.host.MainViewModelFactory
import com.dolgalyova.tasks.screens.host.domain.MainInteractor
import com.dolgalyova.tasks.screens.host.router.AppNavigationHost
import com.dolgalyova.tasks.screens.host.router.AppNavigator
import com.dolgalyova.tasks.screens.host.router.NavigationHost
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    @ActivityScope
    fun interactor(authRepository: AuthRepository, workers: RxWorkers) =
            MainInteractor(authRepository, workers)

    @Provides
    @ActivityScope
    fun navigationHost() = AppNavigationHost()

    @Provides
    @ActivityScope
    fun navigator(host: NavigationHost) = AppNavigator(host)

    @Provides
    @ActivityScope
    fun viewModelFactory(interactor: MainInteractor, navigator: AppNavigator): MainViewModelFactory =
            MainViewModelFactory(interactor, navigator)
}