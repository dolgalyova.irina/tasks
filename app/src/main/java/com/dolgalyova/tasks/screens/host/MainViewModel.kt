package com.dolgalyova.tasks.screens.host

import androidx.lifecycle.ViewModel
import com.dolgalyova.tasks.screens.host.domain.MainInitialScreen
import com.dolgalyova.tasks.screens.host.domain.MainInitialScreen.*
import com.dolgalyova.tasks.screens.host.domain.MainInteractor
import com.dolgalyova.tasks.screens.host.router.AppNavigator
import com.dolgalyova.tasks.screens.host.router.Flow

class MainViewModel(navigator: AppNavigator, interactor: MainInteractor) : ViewModel() {

    init {
        interactor.getFirstScreen {
            when (it) {
                Tasks -> navigator.setFlow(Flow.Main)
                Auth -> navigator.setFlow(Flow.Auth)
            }
        }
    }
}