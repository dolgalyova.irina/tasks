package com.dolgalyova.tasks.screens.host.di

import com.dolgalyova.tasks.common.di.ActivityScope
import com.dolgalyova.tasks.screens.auth.di.AuthFlowComponent
import com.dolgalyova.tasks.screens.host.MainActivity
import com.dolgalyova.tasks.screens.main_flow.di.MainFlowComponent
import dagger.BindsInstance
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [MainModule::class, MainNavigationModule::class])
interface MainComponent {

    fun inject(target: MainActivity)

    fun plusAuthFlow(): AuthFlowComponent.Builder
    fun plusMainFlow(): MainFlowComponent.Builder

    @Subcomponent.Builder
    interface Builder {
        fun module(module: MainModule): Builder
        fun navigationModule(navigationModule: MainNavigationModule): Builder
        @BindsInstance fun target(target: MainActivity): Builder
        fun build(): MainComponent
    }

    interface ComponentProvider {
        fun provideMainComponent(target: MainActivity): MainComponent
    }
}