package com.dolgalyova.tasks.screens.auth.domain.model

inline class Email(val data: String)