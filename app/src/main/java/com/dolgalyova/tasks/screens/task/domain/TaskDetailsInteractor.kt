package com.dolgalyova.tasks.screens.task.domain

import com.dolgalyova.tasks.common.arch.Interactor
import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.model.TaskId
import com.dolgalyova.tasks.common.data.model.TaskInfo
import com.dolgalyova.tasks.common.data.repository.TaskRepository
import com.dolgalyova.tasks.common.util.composeWith

class TaskDetailsInteractor(private val id: TaskId,
                            private val repository: TaskRepository,
                            private val workers: RxWorkers) : Interactor() {

    fun getTask(onSuccess: (TaskInfo) -> Unit, onError: (Throwable) -> Unit) {
        addSubscription(repository.getById(id.id)
                .composeWith(workers)
                .subscribe(onSuccess, onError))
    }

    fun deleteTask(onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        addSubscription(repository.delete(id.id)
                .onErrorComplete()
                .composeWith(workers)
                .subscribe(onSuccess, onError))
    }
}