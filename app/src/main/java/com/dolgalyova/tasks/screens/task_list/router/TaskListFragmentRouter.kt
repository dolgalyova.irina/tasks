package com.dolgalyova.tasks.screens.task_list.router

import com.dolgalyova.tasks.common.data.model.TaskId
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowNavigator
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowNavigator.Screen.*

class TaskListFragmentRouter(private val navigator: MainFlowNavigator) : TaskListRouter {

    override fun exit() = navigator.goBack()

    override fun openTask(id: Int) = navigator.setScreen(TaskDetails, TaskId(id))

    override fun openCreateTask() = navigator.setScreen(EditTask)

    override fun openReminders() = navigator.setScreen(Reminders)
}