package com.dolgalyova.tasks.screens.edit_task

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.tasks.screens.edit_task.di.EditTaskComponent

class EditTaskViewModelFactory(private val component: EditTaskComponent) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            EditTaskViewModel { component.inject(it) } as T
}