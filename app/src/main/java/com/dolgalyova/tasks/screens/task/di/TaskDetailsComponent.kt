package com.dolgalyova.tasks.screens.task.di

import com.dolgalyova.tasks.common.di.ScreenScope
import com.dolgalyova.tasks.screens.task.TaskDetailsViewModel
import dagger.BindsInstance
import dagger.Subcomponent

@ScreenScope
@Subcomponent(modules = [TaskDetailsModule::class])
interface TaskDetailsComponent {

    fun inject(target: TaskDetailsViewModel)

    @Subcomponent.Builder
    interface Builder {
        fun module(module: TaskDetailsModule): Builder
        @BindsInstance
        fun taskId(@TaskIdQualifier taskId: Int): Builder
        fun build(): TaskDetailsComponent
    }

    interface ComponentProvider {
        fun provideTaskDetailsComponent(taskId: Int): TaskDetailsComponent
    }
}