package com.dolgalyova.tasks.screens.edit_task

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dolgalyova.tasks.common.arch.SingleLiveEvent
import com.dolgalyova.tasks.common.data.exception.ItemDoesntExistException
import com.dolgalyova.tasks.common.data.model.Priority
import com.dolgalyova.tasks.common.data.model.TaskInfo
import com.dolgalyova.tasks.screens.edit_task.domain.EditTaskInteractor
import com.dolgalyova.tasks.screens.edit_task.domain.InvalidTitleException
import com.dolgalyova.tasks.screens.edit_task.router.EditTaskRouter
import com.dolgalyova.tasks.screens.task.domain.exception.InvalidDueDateException
import timber.log.Timber
import javax.inject.Inject

class EditTaskViewModel(injector: (EditTaskViewModel) -> Unit) : ViewModel() {
    private val item = MutableLiveData<TaskInfo>()
    val itemObservable: LiveData<TaskInfo>
        get() = item
    @Inject lateinit var interactor: EditTaskInteractor
    @Inject lateinit var router: EditTaskRouter

    private val generalError = SingleLiveEvent<Any>()
    val observeGeneralErrors: LiveData<Any>
        get() = generalError
    private val invalidDateError = SingleLiveEvent<Any>()
    val observeInvalidDateErrors: LiveData<Any>
        get() = invalidDateError
    private val invalidTitleError = SingleLiveEvent<Any>()
    val observeInvalidTitleErrors: LiveData<Any>
        get() = invalidTitleError

    init {
        injector(this)
        interactor.getTask({ item.value = it }, ::onError)
    }

    fun onSaveClick(title: String, priority: Priority?, dueBy: String, hasReminder: Boolean) {
        interactor.saveTask(title, priority, dueBy, hasReminder, ::onTaskSaved, ::onError)
    }

    fun onBackClick() = router.exit()

    private fun onTaskSaved() = router.exit()


    private fun onError(error: Throwable) = when (error) {
        is ItemDoesntExistException -> {
            // do nothing
        }
        is InvalidTitleException->invalidTitleError.call()
        is InvalidDueDateException -> invalidDateError.call()
        else -> generalError.call()
    }.also { Timber.e(error) }
}