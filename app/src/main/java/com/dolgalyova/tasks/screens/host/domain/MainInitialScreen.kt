package com.dolgalyova.tasks.screens.host.domain

enum class MainInitialScreen {
    Tasks, Auth
}