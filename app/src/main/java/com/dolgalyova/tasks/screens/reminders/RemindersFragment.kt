package com.dolgalyova.tasks.screens.reminders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.common.arch.OnBackPressListener
import com.dolgalyova.tasks.screens.reminders.di.RemindersComponent
import com.dolgalyova.tasks.screens.reminders.view.RemindersAdapter
import kotlinx.android.synthetic.main.fragment_reminders.*

class RemindersFragment : Fragment(), OnBackPressListener {

    companion object {
        fun create() = RemindersFragment()
    }

    private val component by lazy {
        (parentFragment as RemindersComponent.ComponentProvider).provideRemindersComponent()
    }
    private val viewModel by lazy {
        ViewModelProviders.of(this, RemindersViewModelFactory(component))
                .get(RemindersViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_reminders, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        viewModel.observeReminders.observe(viewLifecycleOwner,
                Observer { (reminders.adapter as RemindersAdapter).submitList(it) })
        viewModel.observeGeneralErrors.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, R.string.error_general, Toast.LENGTH_SHORT).show()
        })
    }

    override fun onStart() {
        super.onStart()
        viewModel.onLoadMore()
    }

    override fun onBackPress(): Boolean {
        viewModel.onBackClick()
        return true
    }

    private fun initViews() {
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener { viewModel.onBackClick() }

        reminders.layoutManager = LinearLayoutManager(context)
                .apply { orientation = RecyclerView.VERTICAL }
        reminders.adapter = RemindersAdapter(viewModel::onItemClick)
    }
}