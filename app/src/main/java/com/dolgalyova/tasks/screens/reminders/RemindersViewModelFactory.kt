package com.dolgalyova.tasks.screens.reminders

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.tasks.screens.reminders.di.RemindersComponent

class RemindersViewModelFactory(private val component: RemindersComponent) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            RemindersViewModel { component.inject(it) } as T
}