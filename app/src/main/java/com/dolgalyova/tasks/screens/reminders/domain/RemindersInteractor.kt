package com.dolgalyova.tasks.screens.reminders.domain

import com.dolgalyova.tasks.common.arch.Interactor
import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.model.TaskInfo
import com.dolgalyova.tasks.common.data.repository.TaskRepository
import com.dolgalyova.tasks.common.util.composeWith
import com.dolgalyova.tasks.screens.reminders.domain.model.Reminder

class RemindersInteractor(private val tasksRepository: TaskRepository,
                          private val workers: RxWorkers) : Interactor() {

    fun getReminders(onSuccess: (List<Reminder>) -> Unit, onError: (Throwable) -> Unit) {
        addSubscription(tasksRepository.getTasksWithReminder()
                .map { it.map { it.toReminder() } }
                .composeWith(workers)
                .subscribe(onSuccess, onError))
    }
}

private fun TaskInfo.toReminder(): Reminder {
    return Reminder(this.id, this.title, this.dueDate)
}