package com.dolgalyova.tasks.screens.task_list.di

import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.repository.TaskRepository
import com.dolgalyova.tasks.common.di.ScreenScope
import com.dolgalyova.tasks.screens.main_flow.router.MainFlowNavigator
import com.dolgalyova.tasks.screens.task_list.domain.TaskListInteractor
import com.dolgalyova.tasks.screens.task_list.router.TaskListFragmentRouter
import com.dolgalyova.tasks.screens.task_list.router.TaskListRouter
import dagger.Module
import dagger.Provides

@Module
class TaskListModule {
    @Provides
    @ScreenScope
    fun taskListRouter(navigator: MainFlowNavigator): TaskListRouter =
            TaskListFragmentRouter(navigator)

    @Provides
    @ScreenScope
    fun interactor(repository: TaskRepository, workers: RxWorkers) =
            TaskListInteractor(repository, workers)
}