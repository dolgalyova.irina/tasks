package com.dolgalyova.tasks.screens.task_list.router

interface TaskListRouter {
    fun exit()
    fun openTask(id: Int)
    fun openCreateTask()
    fun openReminders()
}