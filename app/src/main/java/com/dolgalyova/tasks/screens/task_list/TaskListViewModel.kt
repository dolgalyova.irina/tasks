package com.dolgalyova.tasks.screens.task_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dolgalyova.tasks.common.arch.SingleLiveEvent
import com.dolgalyova.tasks.common.data.model.SortCriteria
import com.dolgalyova.tasks.common.data.model.SortField
import com.dolgalyova.tasks.common.data.model.SortType
import com.dolgalyova.tasks.common.data.model.TaskInfo
import com.dolgalyova.tasks.screens.task_list.domain.TaskListInteractor
import com.dolgalyova.tasks.screens.task_list.router.TaskListRouter
import timber.log.Timber
import javax.inject.Inject

class TaskListViewModel(injector: (TaskListViewModel) -> Unit) : ViewModel() {
    private val items = MutableLiveData<List<TaskListItem>>()
    val itemsObservable: LiveData<List<TaskListItem>>
        get() = items
    private val errors = SingleLiveEvent<Any>()
    val generalErrorObservable: LiveData<Any>
        get() = errors
    private var isLoading = false
        set(value) {
            field = value
            val currentItems = items.value.orEmpty()
            if (field) {
                val loading = if (currentItems.isEmpty()) TaskListItem.InitialProgress
                else TaskListItem.LoadNextProgress

                items.value = currentItems + loading
            } else {
                items.value = currentItems.filter { it is TaskListItem.Info }
            }
        }
    private var page: Int = 0
    private var sortCriteria = SortCriteria(SortField.TITLE, SortType.ASC)
    @Inject lateinit var interactor: TaskListInteractor
    @Inject lateinit var router: TaskListRouter

    init {
        injector(this)
        loadNextPage()
    }

    override fun onCleared() {
        super.onCleared()
        interactor.cancel()
    }

    fun onTaskClick(task: TaskInfo) = router.openTask(task.id)

    fun onAddTaskClick() = router.openCreateTask()

    fun onLoadMore() = loadNextPage()

    fun onSortFieldSelected(field: SortField) {
        page = 0
        this.sortCriteria = sortCriteria.copy(field = field)
        loadNextPage()
    }

    fun onSortOrderSelected(order: SortType) {
        page = 0
        this.sortCriteria = sortCriteria.copy(type = order)
        loadNextPage()
    }

    fun onOpenRemindersClick() = router.openReminders()

    private fun loadNextPage() {
        if (isLoading) return
        isLoading = true
        page += 1
        interactor.getTaskList(page, sortCriteria, ::onItemsUpdated, ::onError)
    }

    private fun onItemsUpdated(items: List<TaskInfo>) {
        this.items.value = items.map { TaskListItem.Info(it) }
        isLoading = false
    }

    private fun onError(error: Throwable) {
        page -= 1
        isLoading = false
        Timber.e(error)
        errors.call()
    }
}