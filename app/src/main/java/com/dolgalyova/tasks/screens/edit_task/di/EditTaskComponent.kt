package com.dolgalyova.tasks.screens.edit_task.di

import com.dolgalyova.tasks.common.di.ScreenScope
import com.dolgalyova.tasks.screens.edit_task.EditTaskViewModel
import com.dolgalyova.tasks.screens.task.di.TaskIdQualifier
import dagger.BindsInstance
import dagger.Subcomponent

@ScreenScope
@Subcomponent(modules = [EditTaskModule::class])
interface EditTaskComponent {

    fun inject(target: EditTaskViewModel)

    @Subcomponent.Builder
    interface Builder {
        fun module(module: EditTaskModule): Builder
        @BindsInstance fun taskId(@TaskIdQualifier taskId: Int): Builder
        fun build(): EditTaskComponent
    }

    interface ComponentProvider {
        fun provideEditTaskComponent(taskId: Int): EditTaskComponent
    }
}