package com.dolgalyova.tasks.screens.task_list.di

import com.dolgalyova.tasks.common.di.ScreenScope
import com.dolgalyova.tasks.screens.task_list.TaskListViewModel
import dagger.Subcomponent

@ScreenScope
@Subcomponent(modules = [TaskListModule::class])
interface TaskListComponent {

    fun inject(target: TaskListViewModel)

    @Subcomponent.Builder
    interface Builder {
        fun module(module: TaskListModule): Builder
        fun build(): TaskListComponent
    }

    interface ComponentProvider {
        fun provideTaskListComponent(): TaskListComponent
    }
}