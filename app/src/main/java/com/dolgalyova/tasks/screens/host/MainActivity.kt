package com.dolgalyova.tasks.screens.host

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.common.arch.OnBackPressListener
import com.dolgalyova.tasks.screens.auth.di.AuthFlowComponent
import com.dolgalyova.tasks.screens.auth.di.AuthModule
import com.dolgalyova.tasks.screens.host.di.MainComponent
import com.dolgalyova.tasks.screens.host.router.NavigationHost
import com.dolgalyova.tasks.screens.main_flow.MainFlowFragment
import com.dolgalyova.tasks.screens.main_flow.di.MainFlowComponent
import com.dolgalyova.tasks.screens.main_flow.di.MainFlowModule
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainFlowComponent.ComponentProvider,
        AuthFlowComponent.ComponentProvider {

    private val component by lazy {
        (application as MainComponent.ComponentProvider).provideMainComponent(this)
    }

    @Inject lateinit var viewModelFactory: MainViewModelFactory
    @Inject lateinit var navigationHost: NavigationHost

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        component.inject(this)
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        navigationHost.attachHost(this)
    }

    override fun onBackPressed() {
        val topFragment = supportFragmentManager.fragments.firstOrNull { it is OnBackPressListener }
        val processed = if (topFragment?.isVisible == true) {
            (topFragment as? OnBackPressListener)?.onBackPress() ?: false
        } else false
        if (!processed) super.onBackPressed()
    }

    override fun onStop() {
        super.onStop()
        navigationHost.detachHost()
    }

    override fun provideMainFlowComponent(target: MainFlowFragment): MainFlowComponent {
        return component.plusMainFlow()
                .module(MainFlowModule())
                .target(target)
                .build()
    }

    override fun provideAuthComponent(): AuthFlowComponent {
        return component.plusAuthFlow()
                .module(AuthModule())
                .build()
    }
}