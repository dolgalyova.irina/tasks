package com.dolgalyova.tasks.screens.host.domain

import com.dolgalyova.tasks.common.arch.Interactor
import com.dolgalyova.tasks.common.arch.RxWorkers
import com.dolgalyova.tasks.common.data.repository.AuthRepository
import com.dolgalyova.tasks.common.util.composeWith
import timber.log.Timber

class MainInteractor(private val authRepository: AuthRepository,
                     private val workers: RxWorkers) : Interactor() {

    fun getFirstScreen(onSuccess: (MainInitialScreen) -> Unit) {
        addSubscription(authRepository.isUserAuthorized()
                .composeWith(workers)
                .map {
                    if (it) MainInitialScreen.Tasks
                    else MainInitialScreen.Auth
                }
                .subscribe(onSuccess, Timber::e))
    }
}