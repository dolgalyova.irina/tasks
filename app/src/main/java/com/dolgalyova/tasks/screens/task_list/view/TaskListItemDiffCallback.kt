package com.dolgalyova.tasks.screens.task_list.view

import androidx.recyclerview.widget.DiffUtil
import com.dolgalyova.tasks.screens.task_list.TaskListItem

class TaskListItemDiffCallback : DiffUtil.ItemCallback<TaskListItem>() {

    override fun areItemsTheSame(oldItem: TaskListItem, newItem: TaskListItem): Boolean {
        return (oldItem is TaskListItem.Info && newItem is TaskListItem.Info && oldItem.item.id == newItem.item.id) ||
                (oldItem is TaskListItem.LoadNextProgress && newItem is TaskListItem.LoadNextProgress) ||
                (oldItem is TaskListItem.InitialProgress && newItem is TaskListItem.LoadNextProgress)
    }

    override fun areContentsTheSame(oldItem: TaskListItem, newItem: TaskListItem) = oldItem == newItem
}