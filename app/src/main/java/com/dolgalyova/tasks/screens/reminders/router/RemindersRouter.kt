package com.dolgalyova.tasks.screens.reminders.router

interface RemindersRouter {
    fun openDetails(id: Int)

    fun exit()
}