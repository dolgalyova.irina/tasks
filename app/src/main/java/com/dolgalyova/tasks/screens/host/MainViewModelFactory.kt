package com.dolgalyova.tasks.screens.host

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dolgalyova.tasks.screens.host.domain.MainInteractor
import com.dolgalyova.tasks.screens.host.router.AppNavigator

class MainViewModelFactory(private val interactor: MainInteractor,
                           private val navigator: AppNavigator) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            MainViewModel(navigator, interactor) as T
}