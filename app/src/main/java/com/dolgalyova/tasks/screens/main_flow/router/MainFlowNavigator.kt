package com.dolgalyova.tasks.screens.main_flow.router

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dolgalyova.tasks.R
import com.dolgalyova.tasks.common.data.model.TaskId
import com.dolgalyova.tasks.common.util.canHandleNavigation
import com.dolgalyova.tasks.common.util.clearBackStack
import com.dolgalyova.tasks.screens.edit_task.EditTaskFragment
import com.dolgalyova.tasks.screens.main_flow.MainFlowFragment
import com.dolgalyova.tasks.screens.reminders.RemindersFragment
import com.dolgalyova.tasks.screens.task.TaskDetailsFragment
import com.dolgalyova.tasks.screens.task_list.TaskListFragment

class MainFlowNavigator(private val host: MainFlowFragment,
                        private val router: MainFlowRouter) {
    private var pendingScreen: Pair<Screen, Any?>? = null

    fun setScreen(screen: Screen, data: Any? = null) {
        if (host.canHandleNavigation()) {
            val fragment: Fragment? = when (screen) {
                Screen.TasksList -> TaskListFragment.create()
                Screen.TaskDetails -> TaskDetailsFragment.create(data as TaskId)
                Screen.EditTask -> EditTaskFragment.create(data as? TaskId)
                Screen.Reminders -> RemindersFragment.create()
            }
            val addToBackStack = screen != Screen.TasksList
            fragment?.let { setFragment(it, addToBackStack) }

        } else {
            pendingScreen = screen to data
            attachObserver()
        }
    }

    fun goBack() {
        if (host.canHandleNavigation()) {
            val fm = host.childFragmentManager
            if (fm.backStackEntryCount > 0) fm.popBackStack()
            else router.goBack()
        }
    }

    private fun setFragment(child: Fragment,
                            addToBackStack: Boolean,
                            @IdRes containerId: Int = R.id.mainFlowContainer) {
        val fm = host.childFragmentManager
        val transaction = fm
                .beginTransaction()
                .replace(containerId, child, child::class.java.simpleName)

        if (addToBackStack) transaction.addToBackStack(null)
        else fm.clearBackStack()

        transaction.commit()
    }

    private fun attachObserver() {
        host.lifecycle.addObserver(object : LifecycleObserver {

            @OnLifecycleEvent(Lifecycle.Event.ON_START)
            fun dispatchPendingNavigation() {
                pendingScreen?.let { setScreen(it.first, it.second) }
                pendingScreen = null
                host.lifecycle.removeObserver(this)
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun clearObserver() = host.lifecycle.removeObserver(this)
        })
    }

    enum class Screen {
        TasksList, TaskDetails, EditTask, Reminders
    }
}